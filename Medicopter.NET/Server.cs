﻿using System;
using RestSharp;

namespace Medicopter
{
    public class Server
    {
        public string IPAddress { get; set; }
        public int Port { get; set; }
        public int ClientID { get; set; }

        private RestClient client;

        /// <summary>
        /// Connects to a Medicopter server with a user-defined client-id
        /// </summary>
        /// <param name="IPAddress">The IP address of the server</param>
        /// <param name="Port">The port of the server, usually 5000</param>
        /// <param name="ClientID">The ID of the client you wanna update</param>
        public Server(string IPAddress, int Port, int ClientID)
        {
            this.IPAddress = IPAddress;
            this.Port = Port;
            this.ClientID = ClientID;

            client = new RestClient(IPAddress + ":" + Port);
        }

        /// <summary>
        /// Sends a heartbeat to the client specified in the constructor
        /// </summary>
        /// <returns></returns>
        public string SendHeartbeat()
        {
            var request = new RestRequest(String.Format("/api/clients/{0}/heartbeat", ClientID));
            var response = client.Put(request);

            return response.StatusCode.ToString();
        }
    }
}